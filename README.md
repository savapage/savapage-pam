<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

# savapage-pam
                        
This module is used to authenticate a GNU/Linux user with PAM 
(Pluggable Authentication Modules).

The result of the authentication is echoed on `stdout` in JSON format:

    { "valid" : true }

or ...

    { "valid" : false , "error"  : "reason why authentication failed" }

### License

This module is part of the SavaPage project <https://www.savapage.org>,
copyright (c) 2011-2020 Datraverse B.V. and licensed under the
[GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl.html)
version 3, or (at your option) any later version.

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Members. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Dependencies        

The PAM development package is needed to build the binary. Install with this 
command:

    $ sudo apt-get install libpam0g-dev


### Permissions 

Make sure owner and permissions of the `savapage-pam` binary are set as 
`setuid` root (otherwise you will only be able to validate *your own* 
username/password):
 
    $ sudo chown root:root ./savapage-pam
    $ sudo chmod 4511 ./savapage-pam


### Usage 

1. Test manually with just `./savapage-pam`

1. Test in a script with: `echo -e "{user}\\n{password}\\n" | ./savapage-pam` 
   * The `-e` switch enables interpretation of backslash escapes. 
   
1. For system calls from Java, use the `stdin` parameter in the constructor 
   of `SystemCommandExecutor`. Also see: `CommandExecutor#create(p1, p2)`
 

### PAM Configuration

The service name of the module is `savapage`, and the configuration is as 
follows:

    $ ls -l /etc/pam.d/savapage
    -rw-r--r-- 1 root root 92 May  9 14:11 /etc/pam.d/savapage
    
    $ cat /etc/pam.d/savapage
    
    #
    # The PAM configuration file for the Shadow `passwd' service
    #
    
    @include common-password

Note: the content of `/etc/pam.d/savapage` is identical to `/etc/pam.d/passwd`

