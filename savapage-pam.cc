/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */

/**
 * @file savapage-pam.cc
 *
 * @brief Implementation of GNU/Linux authentication based on Pluggable
 * Authentication Modules (PAM).
 */
//-------------------------------------
// C Library
//-------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>

#include <security/pam_appl.h>
#include <security/pam_misc.h>

//-------------------------------------
// Standard C++ Library
//-------------------------------------
#include <string>
#include <iostream>

#define CLI_SWI_DEBUG		"--debug"
#define CLI_SWI_HELP		"--help"
#define CLI_SWI_HELP_SHORT	"-h"

//-------------------------------------
namespace org {
namespace savapage {
namespace pam {

static std::string program_version("1.2.0");
static std::string program_name;

/**
 * @brief
 */
static const std::string kServerName("savapage");

static void ReadLine(char* dest, int max_chars);

static int PasswordConversation(int num_msg, const struct pam_message **msg,
		struct pam_response **resp, void *appdata_ptr);

static struct pam_conv conversation = { &PasswordConversation, NULL };

static void WriteJsonMsg(std::ostream &ostr, bool valid,
		const std::string &error);

/**
 * Parses the program name from the command-line invocation.
 */
static void ParseProgramName(const char* programPath) {

	const char * name;
	for (name = programPath + strlen(programPath);
			name != programPath && *name != '/'; name--)
		;
	if (*name == '/') {
		name++;
	}
	program_name = name;
}

/**
 * Writes program usage to stdout.
 */
static void PrintUsage() {
	printf(
			"____________________________________________________________________________\n"
					"SavaPage Pluggable Authentication Module v%s\n"
					"(c) 2020, Datraverse B.V.\n"
					"\n"
					"License: GNU AGPL version 3 or later <https://www.gnu.org/licenses/agpl.html>.\n"
					"         This is free software: you are free to change and redistribute it.\n"
					"         There is NO WARRANTY, to the extent permitted by law.\n"
					"\n"
					"Usage: %s [option]\n"
					"\n", program_version.c_str(), program_name.c_str());

	printf("%-20s %s\n", CLI_SWI_DEBUG, "Echoes debug messages to stdout.");
	printf("%-20s %s\n\n", CLI_SWI_HELP_SHORT "," CLI_SWI_HELP,
			"Shows this help text.");
}

}
}
}

/**
 *
 */
static void org::savapage::pam::ReadLine(char* dest, int max_chars) {

	char *p;

	/* Read the line */
	if (fgets(dest, max_chars, stdin) == NULL) {
		throw std::string("No input detected");
	}

	if (NULL == strstr(dest, "\n")) {
		throw std::string("Input limit exceeded");
	}

	/* Strip off the new line chars */
	if ((p = strstr(dest, "\n")) != NULL) {
		*p = '\0';
	}

	if ((p = strstr(dest, "\r")) != NULL) {
		*p = '\0';
	}

}

/**
 * @brief Implementation of the PAM conversation function.
 */
static int org::savapage::pam::PasswordConversation(int num_msg,
		const struct pam_message **msg, struct pam_response **resp,
		void *appdata_ptr) {

	if (num_msg != 1 || msg[0]->msg_style != PAM_PROMPT_ECHO_OFF) {
		std::cerr << "ERROR: " << msg[0]->msg << "[" << msg[0]->msg_style << "]"
				<< std::endl;
		return PAM_CONV_ERR;
	}

	if (!appdata_ptr) {
		std::cerr << "ERROR: password not set." << std::endl;
		return PAM_CONV_ERR;
	}

	*resp = (pam_response*) calloc(num_msg, sizeof(struct pam_response));
	if (!*resp) {
		std::cerr << "ERROR: out-of-memory." << std::endl;
		return PAM_CONV_ERR;
	}

	(*resp)[0].resp = strdup((char *) appdata_ptr);
	(*resp)[0].resp_retcode = 0;

	return ((*resp)[0].resp ? PAM_SUCCESS : PAM_CONV_ERR);
}

/**
 *
 */
static void org::savapage::pam::WriteJsonMsg(std::ostream &ostr, bool valid,
		const std::string &error) {

	ostr << "{\"valid\" : ";

	if (valid) {
		ostr << "true";
	} else {
		ostr << "false, \"error\" : \"" << error << "\"";
	}

	ostr << "}" << std::endl;
}

/**
 *
 */
int main(int argc, char *argv[]) {

#ifdef PRODUCT_VERSION
	org::savapage::pam::program_version = PRODUCT_VERSION;
#endif

	org::savapage::pam::ParseProgramName(argv[0]);

	if (argc > 1
			&& (strcmp(CLI_SWI_HELP, argv[1]) == 0
					|| strcmp(CLI_SWI_HELP_SHORT, argv[1]) == 0)) {
		org::savapage::pam::PrintUsage();
		return (EXIT_SUCCESS);
	}

	bool is_debug = (argc > 1 && strcmp(CLI_SWI_DEBUG, argv[1]) == 0);

	try {

		pam_handle_t *pam_hndl = NULL;

		/*
		 * Read username and password from stdin into "C" char array.
		 */
		char username[1024];
		char password[1024];

		org::savapage::pam::ReadLine(username, sizeof(username));
		org::savapage::pam::ReadLine(password, sizeof(password));

		if (*username == '\0') {
			throw std::string("No username provided.");
		}

		if (*password == '\0') {
			throw std::string("No password provided.");
		}

		/*
		 * Step 1: Create the PAM context and initiate the PAM transaction.
		 */
		org::savapage::pam::conversation.appdata_ptr = password;

		int r_value = pam_start(org::savapage::pam::kServerName.c_str(),
				username, &org::savapage::pam::conversation, &pam_hndl);

		if (r_value != PAM_SUCCESS) {
			throw std::string("Failed to create PAM context.");
		}

		/*
		 * Step 2: Authenticate the user with provided password.
		 */
		r_value = pam_authenticate(pam_hndl, 0);

		if (r_value != PAM_SUCCESS) {
			throw std::string(pam_strerror(pam_hndl, r_value));
		}

		/*
		 * Step 3: Determine if the users account is valid. Check for
		 * authentication token and account expiration and verifies access
		 * restrictions.
		 */
		r_value = pam_acct_mgmt(pam_hndl, 0);

		if (r_value != PAM_SUCCESS) {
			throw std::string(pam_strerror(pam_hndl, r_value));
		}

		/*
		 * Step 4: Get record in the password database (e.g., the local password
		 * file /etc/passwd, NIS, and LDAP) that matches the username name.
		 *
		 * INVARIANT: user MUST authenticate with username as found in
		 * the database.
		 */
		struct passwd *password_rec = getpwnam(username);

		if (password_rec != NULL && password_rec->pw_name != (char *) NULL) {
			if (strcasecmp(username, password_rec->pw_name) != 0) {
				throw std::string("Username not found in database.");
			}
		}

		if (pam_hndl != NULL && pam_end(pam_hndl, r_value) != PAM_SUCCESS) {
			pam_hndl = NULL;
		}

		org::savapage::pam::WriteJsonMsg(std::cout, true, "");

	} catch (const std::string & error) {

		if (is_debug) {
			std::cerr << error << std::endl;
		}

		org::savapage::pam::WriteJsonMsg(std::cout, false, error);
	}

	exit(EXIT_SUCCESS);
}

/* end-of-file */
