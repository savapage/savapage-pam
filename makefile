#
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2011-2020 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
#

#-------------------------------------------------------------------------
# Prerequisite for make:
# 	$ sudo apt-get install libpam0g-dev
#-------------------------------------------------------------------------

#-------------------------------------------------------------------------
# 'make' command line parameters:
# 	PRODUCT_VERSION=x.x.x
#-------------------------------------------------------------------------

LIBS_CLIENT := -lpam

LIBS_CLIENT += -Wl,-Bsymbolic-functions

#
CFLAGS := 

ifdef PRODUCT_VERSION 
CFLAGS += -DPRODUCT_VERSION=\"$(PRODUCT_VERSION)\"
endif

#
# Static link of C and C++ libs. 
#
LDFLAGS := -static-libstdc++ -static-libgcc

MY_PROGRAM := savapage-pam

TARGET_DIR=./target

.PHONY: all
all: init $(TARGET_DIR)/$(MY_PROGRAM)

.PHONY: init
init:
	@mkdir -p $(TARGET_DIR)  

$(TARGET_DIR)/$(MY_PROGRAM) : $(TARGET_DIR)/$(MY_PROGRAM).o
	g++ $^ $(LIBS_CLIENT) $(LDFLAGS) -o $@ 
	strip $@

$(TARGET_DIR)/$(MY_PROGRAM).o : $(MY_PROGRAM).cc
	g++ -c $(INCLUDES) $(CFLAGS) -o $@ $< 

.PHONY: clean
clean:
	@rm -f $(TARGET_DIR)/$(MY_PROGRAM).o $(TARGET_DIR)/$(MY_PROGRAM)

# end-of-file
